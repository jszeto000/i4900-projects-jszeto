#include "ske.h"
#include "prf.h"
#include <openssl/sha.h>
#include <openssl/evp.h>
#include <openssl/hmac.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* memcpy */
#include <errno.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#ifdef LINUX
#define MMAP_SEQ MAP_PRIVATE|MAP_POPULATE
#else
#define MMAP_SEQ MAP_PRIVATE
#endif

/* NOTE: since we use counter mode, we don't need padding, as the
 * ciphertext length will be the same as that of the plaintext.
 * Here's the message format we'll use for the ciphertext:
 * +------------+--------------------+-------------------------------+
 * | 16 byte IV | C = AES(plaintext) | HMAC(C) (32 bytes for SHA256) |
 * +------------+--------------------+-------------------------------+
 * */

/* we'll use hmac with sha256, which produces 32 byte output */
#define HM_LEN 32
#define KDF_KEY "qVHqkOVJLb7EolR9dsAMVwH1hRCYVx#I"
/* need to make sure KDF is orthogonal to other hash functions, like
 * the one used in the KDF, so we use hmac with a key. */

int ske_keyGen(SKE_KEY* K, unsigned char* entropy, size_t entLen)
{
	if (entropy == NULL)
	{
		randBytes(K->hmacKey,KLEN_SKE);
		randBytes(K->aesKey,KLEN_SKE);
	} 
	else 
	{
		unsigned char kdf_SK[KLEN_SKE*2];
		HMAC(EVP_sha512(), KDF_KEY, HM_LEN, entropy, entLen, kdf_SK, NULL);
		int i;
		for(i = 0; i < 32; i++)
		{
			K->hmacKey[i] = kdf_SK[i];
			K->aesKey[i] = kdf_SK[i+KLEN_SKE];
		}
		printf("KDF_SK:   %64s\n", kdf_SK);
		printf("K->hmac:  %32s\n", K->hmacKey);
		printf("K->aes:   %32s\n", K->aesKey);

	}
	/* TODO: write this.  If entropy is given, apply a KDF to it to get
	 * the keys (something like HMAC-SHA512 with KDF_KEY will work).
	 * If entropy is null, just get a random key (you can use the PRF). */
	return 0;
}
size_t ske_getOutputLen(size_t inputLen)
{
	return AES_BLOCK_SIZE + inputLen + HM_LEN;
}
size_t ske_encrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len,
		SKE_KEY* K, unsigned char* IV)
{
	unsigned char* ct = malloc(ske_getOutputLen(len));
	unsigned char* hmac_ct = malloc(HM_LEN);
	memset(hmac_ct, 0, HM_LEN);
	//AES on the plaintext
	if(IV == NULL || strlen((char*)IV) == 0)
	{
		IV = malloc(16);
		randBytes(IV,16);
	} 

	EVP_CIPHER_CTX* ctx = EVP_CIPHER_CTX_new();
	if (1!=EVP_EncryptInit_ex(ctx,EVP_aes_256_ctr(),0,K->aesKey,IV))
		ERR_print_errors_fp(stderr);
	int nWritten;
	if (1!=EVP_EncryptUpdate(ctx,ct,&nWritten,inBuf,len))
		ERR_print_errors_fp(stderr);
	EVP_CIPHER_CTX_free(ctx);
	size_t ctLen = nWritten;
	memcpy(outBuf, IV, 16);	
	memcpy(outBuf+16, ct, ctLen);
	HMAC(EVP_sha256(), K->hmacKey, KLEN_SKE, outBuf, ctLen+16, hmac_ct, NULL);
	//printf("enc hmac_ct: %s\n", hmac_ct);
	memcpy(outBuf+16+ctLen, hmac_ct, HM_LEN);

	/* TODO: finish writing this.  Look at ctr_example() in aes-example.c
	 * for a hint.  Also, be sure to setup a random IV if none was given.
	 * You can assume outBuf has enough space for the result. */
	/* TODO: should return number of bytes written, which hopefully matches ske_getOutputLen(...). */
	
	return ctLen+HM_LEN+16; 
	
}
size_t ske_encrypt_file(const char* fnout, const char* fnin,
		SKE_KEY* K, unsigned char* IV, size_t offset_out)
{
	int fd = open(fnin, O_RDONLY);
	unsigned char* map;
	map = mmap(0, 0x1000, PROT_READ, MAP_SHARED,fd,0);
	size_t ctLen = strlen((const char*)map);
	unsigned char* ct = malloc(ctLen);
	size_t nBytes;
	nBytes = ske_encrypt(ct, map, ctLen, K, IV);
	close(fd);
	if(!offset_out)
		fd = open(fnout,O_RDWR | O_CREAT | O_TRUNC, (mode_t)0600);
	else
		fd = open(fnout,O_RDWR | O_CREAT, (mode_t)0600);
	lseek(fd,offset_out,SEEK_SET);
	write(fd,ct,nBytes);
	close(fd);
	/* TODO: write this.  Hint: mmap. */
	return nBytes;
}
size_t ske_decrypt(unsigned char* outBuf, unsigned char* inBuf, size_t len,
		SKE_KEY* K)
{
	//unsigned char* hmac_ct = malloc(HM_LEN+1);
	//unsigned char* hmac_check = malloc(HM_LEN+1);
	unsigned char hmac_ct[HM_LEN+1];
	unsigned char hmac_check[HM_LEN+1];
	memset(hmac_ct, 0, HM_LEN+1);
	memset(hmac_check, 0, HM_LEN+1);
	unsigned char* IV = malloc(16);
	EVP_CIPHER_CTX* ctx = EVP_CIPHER_CTX_new();
	//hmac which is the last 32 bytes of the ciphertext
	memcpy(hmac_check, inBuf+(len-HM_LEN), HM_LEN);
	memcpy(IV, inBuf, 16);
	size_t ctLen = len-HM_LEN-16;
	unsigned char* ct = malloc(ctLen);
	memcpy(ct, inBuf+16, ctLen);
	unsigned char* IV_ct = malloc(len-HM_LEN);
	memcpy(IV_ct, inBuf, ctLen+16);
	//hmac which is derived from the IV and ct
	HMAC(EVP_sha256(), K->hmacKey, HM_LEN, IV_ct, len-HM_LEN, hmac_ct, NULL);
	
	int i;
	for(i = 0; i < HM_LEN; i++)
	{
		if(hmac_ct[i] != hmac_check[i])
		{	//printf("hmac_ct & hmac_check don't match\n");
			//printf("hmac_ct: %su\n\n", hmac_ct);
			//printf("hmac_check: %su\n", hmac_check);
			return -1;
		}
	}
	if (1!=EVP_DecryptInit_ex(ctx,EVP_aes_256_ctr(),0,K->aesKey,IV))
		ERR_print_errors_fp(stderr);
	int nWritten;
	if (1!=EVP_DecryptUpdate(ctx,outBuf,&nWritten,ct,ctLen))
		ERR_print_errors_fp(stderr);
	size_t ptLen = nWritten;
	//EVP_CIPHER_CTX_free(ctx);
	
	/* TODO: write this.  Make sure you check the mac before decypting!
	 * Oh, and also, return -1 if the ciphertext is found invalid.
	 * Otherwise, return the number of bytes written.  See aes-example.c
	 * for how to do basic decryption. */
	return ptLen;
}
size_t ske_decrypt_file(const char* fnout, const char* fnin,
		SKE_KEY* K, size_t offset_in)
{
/*	unsigned char* map;
	int i;
	FILE *fin;
	fin = fopen(fnin,"r");
	fseek(fin,0,SEEK_END);
	size_t ctLen = ftell(fin);
	unsigned char* pt = malloc(ctLen);
	map = malloc(ctLen);
	rewind(fin);
	fread(map,1,ctLen,fin);
	size_t nBytes;
	close(fin);
	nBytes = ske_decrypt(pt, map, ctLen, K);
	//int fd = open(fnout,O_RDWR | O_CREAT | O_TRUNC, (mode_t)0600);
	int fd = open(fnout, O_RDWR | O_CREAT | O_TRUNC, 00666);
	lseek(fd,offset_in,SEEK_SET);
	write(fd,pt,nBytes);
	ftruncate(fd,nBytes);
	close(fd);

	return nBytes;
*/
	int fd = open(fnin, O_RDONLY);
	unsigned char* map;
	map = mmap(0, 0x1000, PROT_READ, MAP_SHARED,fd,0);
	size_t ctLen = strlen((const char*)map);
	unsigned char* dt = malloc(ctLen);
	size_t nBytes;
	nBytes = ske_decrypt(dt, map+offset_in, ctLen, K);
	close(fd);
//	if(!offset_in)
//		fd = open(fnout,O_RDWR | O_CREAT | O_TRUNC, (mode_t)0600);
//	else
		fd = open(fnout,O_RDWR | O_CREAT, (mode_t)0600);
	write(fd,dt,nBytes);
	close(fd);
	/* TODO: write this.  Hint: mmap. */
	return nBytes;
}
